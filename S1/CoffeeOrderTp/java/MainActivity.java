package com.example.coffeeordertp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.Normalizer;
import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {

    private Button buttonPlus, buttonMinus, buttonOrder;
    private TextView textViewNumber, textViewResult;
    private CoffeeOrder myCoffeeOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeUI();
        myCoffeeOrder = new CoffeeOrder(1.2);

        buttonPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCoffeeOrder.addCoffee();
                displayNumber(myCoffeeOrder.getQuantity());
            }
        });

        buttonMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCoffeeOrder.removeCoffee();
                displayNumber(myCoffeeOrder.getQuantity());
            }
        });

        buttonOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayTotalOrder(myCoffeeOrder.getOrderPrice());
            }
        });

    }

    private void displayNumber(int aQuantity){
        textViewNumber.setText(Integer.toString(aQuantity));
    }

    private void displayTotalOrder(Double aTotal){
        textViewResult.setText(NumberFormat.getCurrencyInstance().format(aTotal));

    }


    private void initializeUI(){
        buttonPlus = findViewById(R.id.buttonPlus);
        buttonMinus = findViewById(R.id.buttonMinus);
        buttonOrder = findViewById(R.id.buttonOrder);
        textViewNumber = findViewById(R.id.textViewNumber);
        textViewResult = findViewById(R.id.textViewResult);
    }
}

