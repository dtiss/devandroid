package com.example.coffeeordertp;

class CoffeeOrder {

    private Double price;
    private int quantity;

    public CoffeeOrder(Double aPrice){
        if(aPrice <0.00){
            this.price = 0.00;
        }else{
            this.price = aPrice;
        }
        this.quantity = 0;
    }

    public void addCoffee(){
        this.quantity+= 1;
    }

    public void removeCoffee(){
        if(this.quantity>0){
            this.quantity-=1;
        }
    }

    public int getQuantity(){
        return this.quantity;
    }

    public Double getOrderPrice(){
        Double result =0.00;
        result = this.price * this.quantity;
        return result;
    }
}

